<?php

class Controller_Message extends LctController {
	
	private static $tabMenu;
	
	public function before(){		
		self::$tabMenu = array(
				array('url'=>'message/inbox','title'=>'_{inbox}','suffix'=>'(120)'),
				array('url'=>'message/outbox','title'=>'_{outbox}','suffix'=>'(50)'),
				array('url'=>'message/marking','title'=>'_{marking}','suffix'=>'(75)'),
				array('url'=>'message/system','title'=>'_{system}','suffix'=>'(120)'),
				array('url'=>'message/dustbin','title'=>'_{dustbin}','suffix'=>'(12)'));
		parent::before();
	}
	
	public function after(){
		if(!Request::current()->is_ajax()){
			$this->view->extend('profile/page');
			$this->view->extend('layout/page');
			$this->view->tabMenus = self::$tabMenu;
		}
		parent::after();
	}

	/**
	 *
	 */
	public function action_index(){
			$this->action_inbox();
	}

	/**
	 *
	 */
	public function action_setvisible(){
		$letter_id = explode("_",Request::current()->query("id"));
		$a = models\Messagerepository::getAuctionOwner($letter_id[1]);
		if(Lct::client()->loggedIn() && $a[0]['id'] == Lct::client()->id){
			echo models\Messageinbox::setFrontendVisible($letter_id[1]);
		}
	}

	/**
	 *
	 */
	public function action_renderlist(){
		$this->view->auction_id = $_GET['auction_id'];
		$this->view->auctionOwner_id = $_GET['auctionOwner_id'];
		$this->view->productMessageList = models\Messagerepository::getProductMessageList($_GET['auction_id']);
		$this->view->setFile('message/renderlist');
	}

	/**
	 *
	 */
	public function action_renderform(){
		if(Request::current()->is_ajax()){
			if(!Lct::client()->loggedIn()){
				echo json_encode(array('loggedIn'=>false,'message'=>'not-logged-in'));return;
			}
			// Get auctionDataSource
			$auctionDataSource = Doctrine::em('s')->getRepository('models\Auction')->find($_POST["auction_id"]);
			
			$auctionOwnerId = $auctionDataSource->getOwner()->getId();
			$this->view->setFile('message/renderform');
			if(Lct::client()->id != $auctionOwnerId){
				$this->view->formType = "questioner";
			}
			if(Lct::client()->id == $auctionOwnerId){
				$this->view->formType = "seller";
			}
			if($_POST['parent_id'] == null){
				$this->view->formType = "subject";
			}
			
			$this->view->form = Uniform::factory('replymsg');
			$this->view->form->field('auction_id')->value($_POST["auction_id"]);
			$this->view->form->field('locale')->value(Lct::client()->getLanguage());
			
		}
	}

	/**
	 * @throws HTTP_Exception_404
	 */
	public function action_sendmessage(){
		
		if(Request::current()->is_ajax()){
			$this->view->form = Uniform::factory('replymsg');		
			if(count(Request::current()->post()) && Lct::client()->loggedIn()){
				$this->view->form->bind(Request::current()->post());
				// Get auctionDataSource
				$auctionDataSource = Doctrine::em('s')->getRepository('models\Auction')->find($this->view->form->field('auction_id')->value());
				// If isset parent then...Get parent data...
				if($this->view->form->field('parent_id')->value() != null){
					$parentLetterDataSource = Doctrine::em('s')->getRepository('models\Messageinbox')->findOneByLetter($this->view->form->field('parent_id')->value());
					if($parentLetterDataSource->getLetter()->getSender()->getId() != Lct::client()->id && $parentLetterDataSource->getOwner()->getId() != Lct::client()->id){
						throw new HTTP_Exception_404('You are not the owner of the letter, not the sender: '.$this->view->form->field('parent_id')->value());
					}
				}
				$auctionOwnerId = $auctionDataSource->getOwner()->getId();
				if($this->view->form->check() === false && $this->view->form->logicalCheck(array(
																								'auctionDataSource'=>$auctionDataSource
																								)) === false)
				{
					
					if(Lct::client()->id != $auctionOwnerId){
						$this->view->formType = "questioner";
					}
					if(Lct::client()->id == $auctionOwnerId){
						$this->view->formType = "seller";
					}
					if($this->view->form->field('parent_id')->value() == ''){
						$this->view->formType = "subject";
					}
					$this->view->setFile('message/renderform');	
				}else{	// Save to repository				
						$repo = new models\Messagerepository();
						$repo->bind($this->view->form->as_array());
						Doctrine::em('m')->persist($repo);
						Doctrine::em('m')->flush();
						if($this->view->form->field('parent_id')->value() == null){
							$insertId = $repo->getId();
							$repo = Doctrine::em('m')->getRepository('models\Messagerepository')->find($insertId);
							$repo->setStarter(Doctrine::em('m')->getReference('\models\Messagerepository',$insertId));
							Doctrine::em('m')->persist($repo);
							Doctrine::em('m')->flush();
							$ownerId = $auctionOwnerId;
						}else{
							$ownerId = $parentLetterDataSource->getLetter()->getSender()->getId();
						}
						// Save to inbox
						models\Messageinbox::saveMessage(array('letter_id'=>$repo->getId(),'owner_id'=>$ownerId));
						// Save to outbox
						models\Messageoutbox::saveMessage(array('letter_id'=>$repo->getId(),'owner_id'=>Lct::client()->id));
						Doctrine::getCache()->delete('messages_'.$this->view->form->field('auction_id')->value());
						echo json_encode(array('auctionOwnerId'=>$auctionOwnerId));return;
				}
			}else{
				echo json_encode(array('loggedIn'=>false,'message'=>'not-logged-in'));return;
			}
		}
	}

	/**
	 * mail lister
	 * @param <int> $ownerId
	 * @param <string> $query
	 * @return type 
	 */
	public function getMessageListerWidget($params,$query,$box){
		return Lct::widget('DQLLister',array(
			'itemurl'=>url::site('message/[id]'),
			'itemRenderCallback'=>function($record)use($box){
				static $i;
				LctView::factory('message/messagePreview',array(
					'inbox'=>$record,
					'repo'=>$record->getLetter(),
					'category'=>$record->getLetter()->getCategory(),
					'auction'=>$record->getLetter()->getAuction(),
					'sender'=>$record->getLetter()->getSender(),
					'box'=>$box,
					'i'=>$i
				))->render();
				$i++;
			},
			// message widget template		
			'template'=>'widget/lister/righttop',
			'dql'=>sprintf($query,$params['owner_id']),
			'params'=>Request::current()->query(),
		));
	}	
		
	/**
	 *  Inbox
	 */
	public function action_inbox(){
		if(!Lct::client()->loggedIn()){Request::current()->redirect();}
		LctLayout::instance()->addLESS('message/widget/profileindex');
		$this->view->setFile('message/index');
		if(Lct::client()->loggedIn()){
			// default: inbox
			$this->view->lister = $this->getMessageListerWidget(array('owner_id'=>Lct::client()->id),'
					SELECT ibox
					FROM  models\Messageinbox ibox
						JOIN ibox.letter repo
						JOIN repo.sender sender
						JOIN repo.category category
						JOIN repo.auction auction
					WHERE ibox.owner = %d AND
						repo.createdat = (
						SELECT MAX(r.createdat) mx FROM models\Messagerepository r
							WHERE
							r.sender = sender AND
							r.category = repo.category AND
							r.auction = repo.auction AND
							r.starter = repo.starter
					) ORDER BY repo.createdat DESC','inbox');
		}
	}

	/**
	 *  Outbox
	 */
	public function action_outbox(){
		if(!Lct::client()->loggedIn()){Request::current()->redirect();}	
		LctLayout::instance()->addLESS('frontend/message/profileindex');
		$this->view->setFile('message/index');
		if(Lct::client()->loggedIn()){
			// default: outbox
			$this->view->lister = $this->getMessageListerWidget(array('owner_id'=>Lct::client()->id),'
					SELECT outbox
					FROM  models\Messageaoutbox outbox
						JOIN outbox.letter repo
						JOIN repo.sender sender
						JOIN repo.category category
						JOIN repo.auction auction
					WHERE outbox.owner = %d AND
						repo.createdat = (
						SELECT MAX(r.createdat) mx FROM models\Messagerepository r
							WHERE
							r.sender = sender AND
							r.category = repo.category AND
							r.auction = repo.auction AND
							r.starter = repo.starter
					) ORDER BY repo.createdat DESC','outbox');
		}
	}
	
	/**
	 * View a letter
	 * @param <int> $letterId 
	 */
	public function action_view($letterId){
		if(!Lct::client()->loggedIn()){Request::current()->redirect();}
		if(Lct::client()->loggedIn()){
			LctLayout::instance()->addLESS('frontend/message/profileindex');
			$this->view->setFile('message/view');
			$userId = Lct::client()->id;
			// Check that I am the owner of the mail
			$check = false;
			switch(Request::current()->query('from')){
				case'inbox':
					$res = models\Messageinbox::getMessage($letterId,$userId);
					break;
				case'outbox':
					$res = models\Messageoutbox::getMessage($letterId,$userId);
					break;
				case'system':
					$res = models\Messagesystem::getMessage($letterId,$userId);
					break;
				case'dustbin':
					$res = models\Messagedustbin::getMessage($letterId,$userId);
					break;
				default:
					$res = models\Messageinbox::getMessage($letterId,$userId);
			}
			if($res){
				// Add tab menu
				self::$tabMenu = array_merge(self::$tabMenu,
						array(
							array('url'=>'message/view/'.$letterId.'?from='.Request::current()->query('from'),
								'title'=>'_{message}'),
							));
				$this->view->auction = $res[0]->getLetter()->getAuction();
				$this->view->repo = $res[0]->getLetter();
				$this->view->messageFamilyList = models\Messageinbox::getMessageFamilyList(
																		$this->view->auction->getId(),
																		Lct::client()->id,
																		$this->view->repo->getStarter()->getId()	
																		);
				
				
			}else{
				throw new HTTP_Exception_404('The requested message was not found: '.$letterId);
			}
		}else{
			Request::current()->redirect('/');
		}
	}	
}
