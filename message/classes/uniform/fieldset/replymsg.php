<?php
class Uniform_Fieldset_Replymsg extends Uniform_Fieldset {
	
	public function __construct()
	{
	    parent::__construct();
		
		$this->add_field('parent_id')->type('hidden')->value(null);
		$this->add_field('auction_id')->type('hidden');
		$this->add_field('locale')->type('hidden');
			
	    $this->add_field('category_id')->hname('_{category}')->params(array(
				'options'=>"SELECT id,CONCAT('_{',name_id,'}') FROM messageCategory",
		))->type('select')->value(1);
		$this->add_field('othersubject')->hname()->params(array('maxlength'=>'50'))->type('string')->value(null);
	    $this->add_field('content')->hname('_{new-messages}')->params()->type('text');	        
	}
}
