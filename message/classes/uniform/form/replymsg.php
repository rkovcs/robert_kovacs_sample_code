<?php

class Uniform_Form_Replymsg extends Uniform_Form {
	public $fieldsets = array(
	    'replymsg' => array(
	    	'parent_id','auction_id','locale','category_id','othersubject','content'
		),
	);
	
	public function initialize(){
		$form = &$this;
	    $this->field('category_id')->rule('not_empty');  
	    $this->field('content')->rule('not_empty')->rule(function()use($form){
	
			if(models\Messagerepository::isSenderParent($form->field('parent_id')->value())){
				$form->field('othersubject')->add_errors('_{do_not_write_yourself_a_letter}');
				return false;
			}else{
				return true;
			}
		});
	}
	
	public function logicalCheck($params){
		$form = &$this;
		if($params['auctionDataSource']->getOwner()->getId() == Lct::client()->id && $form->field('parent_id')->value() == ""){
			$form->field('othersubject')->add_errors('_{you_can_not_ask_about_your_own_auction}');
			return false;
		}else{
			return true;
		}
	}
	
	
}