<?php

class Widget_Message_Productmessage extends LctWidget{
	
	public function __construct($options){
		LctLayout::instance()->addLESS('frontend/message/productpage');
		parent::__construct($options);
		$this->view->setFile('widget/message/productmessage');
		$this->view->productMessageList = models\Messagerepository::getProductMessageList($options['auction_id']);
		$this->view->auctionOwner_id = $options['auctionOwner_id'];
	}
}