<?php

class Widget_Message_List extends LctWidget{
	
	public function __construct($dataSource,$options){
		parent::__construct($options);
		LctLayout::instance()->addJS('message/widget/list');
		$this->view->setFile('widget/message/list');
		$this->view->auctionOwner_id = $options['auctionOwner_id'];
		$this->view->auction_id = $options['auction_id'];
		$this->view->messagelist = models\Messagerepository::getList($options['auction_id'],$dataSource);
	}
	
	public function getChild($children,$auctionOwner_id){
		$childrenView = '';
		$children = array_reverse($children);
		foreach($children AS $index => $child){
			
			$childrenView.=LctView::factory('widget/message/child',array(
						'child'=>$child,
						'index'=>$index,
						'auctionOwner_id'=>$auctionOwner_id
					));
			if(isset($child['child'])){
				$this->getChild($child['child'],$auctionOwner_id);
			}
			
		}
	}
}