<?php if($content)echo $content."<hr>"; ?>
<?php if(!empty($document) && $document->getId()){ ?>
<h1>
	_{Edit document}: <strong><?php echo $document->getIdtext();?></strong>
	
</h1>
&nbsp;
<?php echo LctWidget::factory('languageselector',array(
	'active'=>$language,
	'url'=>url::site('admin/document/edit/'.$document->getId().'/[idText]'),
));?>
<?php }else{ ?>
<h1>_{Create document}</h1>
<?php };?>
<div class="clear"></div>
<?php echo $form; ?>
<?php if(!empty($document)){ ?>
<input type="button" value="_{delete}" style="float:left;" onclick="Lct.confirmRedirect('<?php 
	echo url::site('admin/document/delete/'.$document->getId());
?>','_{Are you sure you want to delete this document}?')"></input>
<?php }; ?>