<?php Debug::log('layout', Controller_Admin_document::getCategoryId()); ?>

<table width="100%" border="1">
	<tbody valign="top">
		<tr>
			<td colspan="2"><?php 
				$treeWidget->renderBreadcrumb();
				if(Controller_Admin_Document::getCategoryId() > 1 ){
				?>
				<input type="button" value="_{delete}" onclick="Lct.confirmRedirect('<?php 
					echo url::site('admin/document/category_delete/'.Controller_Admin_Document::getCategoryId());
				?>','_{Are you sure you want to delete the category}?')" />
				<input type="button" value="_{edit}" onclick="Lct.redirect('<?php 
					echo url::site('admin/document/category_edit/'.Controller_Admin_Document::getCategoryId());
				?>','_{Are you sure you want to delete the category}?')" />
				<?php 					
				}

			?></td>
		</tr>
		<tr>
			<td width="1%">
				<?php $treeWidget->render();?>
				<?php 
				echo html::anchor(url::site('admin/document/category_edit/new').url::query(array(
					'parent_id'=>Controller_Admin_Document::getCategoryId(),
				),false),'_{create-new}');
				?>
			</td>
			<td><?php 
				echo $content;
			?></td>
		</tr>
	</tbody>
</table>