<div class="content lm_content">	
	<div class="left_side">
	 	<div class="m_header">
	 		<h2>_{anchors}</h2>
	 	</div>
     	<div class="ls_place">
			<?php $document->renderAnchors(); ?>
		</div>
	</div>
	<div class="main">
	    <div class="m_header">
	   		<h2><?php echo $document->getTitle(); ?></h2>
	   	</div>
	    <div class="top_patch"></div>
	    		
		<div class="m_l_block" style="padding:10px;">
				<?php echo $document->getContent(); ?>
		</div>		
	</div>	
	<div class="clear"></div>		
</div>