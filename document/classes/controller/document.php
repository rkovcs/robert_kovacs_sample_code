<?php

class Controller_Document extends LctController{
	
	public function after(){
		$this->view->extend('page');
		$this->view->extend('layout/page');		
		parent::after();
	}
	
	public function action_view($id){
		$this->view->setFile('document/view');
		$this->view->document = LctWidget::factory('document',$id);
	}
	
	public function action_edit($id){
		$widget = LctWidget::factory('document',$id);
		$document = $widget->getDocument();
		if(!empty($document)){
			$url = url::site('admin/document/edit/'.$document->getDocument()->getId().'/'.Kohana::config('db_language.'.$document->getLanguage()->getId().'.idText'));
			Request::current()->redirect($url.url::query(array('ref'=>'frontend')));
		}
	}
}