<?php

class Controller_Admin_Document extends Controller_Admin{
	protected static $_treeWidget;
	public static $_categoryId = null;
	
	public static function menu($url = null){
		$menu = array(
			'title'=>'_{documents}',
			'url'=>$url,
			'items'=>array(
				'list'=>array(
					'title'=>'_{list}',
					'url'=>url::site('admin/document/list'),
				),
				'create'=>array(
					'title'=>'_{create}',
					'url'=>url::site('admin/document/edit/new'),
				)
			)
		);
		return $menu;
	}
	
	public function getDocumentListerWidget(){
		return LctWidget::factory('DQLLister',array(
			'itemurl'=>url::site('admin/document/edit/[id]'),
			'pageurl'=>url::site('admin/document/list/?category_id='.Controller_Admin_Document::$_categoryId),
			'itemRenderCallback'=>function($document){			
				if($document instanceof models\Document){
					$p = array(
						'document'=>$document,
						'printHeader'=>true,
						'url'=>url::site('admin/document/edit/'.$document->getId().'/'.Lct::client()->getLanguage('idText')),
					);
				}else{
					if(!$document->getContent())return;
					$p = array(
						'content'=>$document,
						'printHeader'=>false,
						'url'=>url::site('admin/document/edit/'.$document->getDocument()->getId().'/'.Kohana::config('db_language.'.$document->getLanguage()->getId().'.idText')),
					);
				}
				LctView::factory('admin/document/documentPreview',$p)->render();
			},
			'template'=>'widget/dqllister/basic',
			'dql'=>sprintf(
				'SELECT d,dc FROM models\Document d, models\Documentcontent dc WHERE dc.document = d.id AND d.documentcategory = %d ORDER BY d.idtext',
				Controller_Admin_Document::getCategoryId()
			),
			'params'=>Request::current()->query()
		));
	}
	
	public static function getCategoryId(){
		return self::$_categoryId;
	}
	
	public static function _getCategoryManager(){
		return models\Documentcategory::getNestedsetManager();
	}
	
	public function after(){
		$this->view->extend(
			LctView::factory(
				'admin/document/layout',
				array(
					'treeWidget'=>$this->_getTreeWidget(),
				)
			)
		);
		parent::after();
	}
	
	
	/** 
	 * This method will initialize the tree, if neccessary
	 */
	public function action_inittree(){ 
		echo "<h1>categories</h1>";
		models\Documentcategory::initTree();			
	}
	
	
	protected function _getTreeWidget(){
		if(!self::$_treeWidget){
		 self::$_treeWidget = Lctwidget::factory('treebrowser',array(
				'manager'=>self::_getCategoryManager(),
				'rootId'=>1,
		 		'renderRoot'=>1,
		 		'titleFormatter'=>function($node){
		 			//echo \Debug::vars($node->getName()->getIdentifier());
		 			return '_{'.$node->getName()->getIdentifier().'}';
		 		},
		 		'expandTo' => ( self::$_categoryId ? self::$_categoryId : null ),
				//'dataSource'=>url::site('admin/document/ds_categories'),
				'url'=>url::site('admin/document/list/?category_id=[id]'),
				'contextMenu'=>array(
					array(
						'title' => '_{edit}',
						'url'=>url::site('admin/document/categoryedit/[id]'),
					),
					array(
						'title' => '_{create child}',
						'url'=>url::site('admin/document/categoryedit/new?parent_id=[id]'),
					),
					array(
						'title' => '_{delete}',
						'url'=>url::site('admin/document/categorydelete/[id]'),
						'class'=>'confirm',
					),
				),
			));
		};
		return self::$_treeWidget;
	}
	
	public function action_ds_categories(){
		$widget = $this->_getTreeWidget()->dataSource(Request::current()->query());
	}
	
	public function action_index(){
		$this->view->setFile('admin/document/index');	
	}
	
	public function action_category_edit($id = 'new'){
		$events = array();
		$bind = array();
		if($id=='new'){			
			if(!Request::current()->query('parent_id'))throw new Exception('You must specify parent id');
			self::$_categoryId =  Request::current()->query('parent_id');
			$bind['parent_id'] = self::$_categoryId;
			$events['insteadofsave'] = function(&$record,&$form){
				$parent = Doctrine::em('s')->getRepository('models\Documentcategory')->findOneById(Controller_Admin_Document::$_categoryId);
				$parentNode = new DoctrineExtensions\NestedSet\NodeWrapper($parent,Controller_Admin_Document::_getCategoryManager());
				$parentNode->addChild($record);
			};
		}else{
			self::$_categoryId = $id;
			$events['insteadofsave'] = function(&$record,&$form){
				$data = $form->as_array();
				$name = $data['name']; unset($data['name']);
				$langId = $data['language_id']; unset($data['language_id']);
				$record->bind($data);
				$dw = $record->getName();
				$dw->setTranslation($langId,$name);			
				Doctrine::em('m')->persist($record);
				Doctrine::em('m')->flush();
			};
			$events['beforeshow'] = function(&$record,&$form)use($id){
				if(Request::current()->query('language_id')){
					$langId = Request::current()->query('language_id');
				}else{
					$langId = Lct::client()->getLanguage('idText'); 
				}
				$form->field('language_id')->value($langId);
				if($langId){
					$translation = $record->getName()->getTranslation($langId);
					$form->field('name')->value($translation);
				};	
				$form->field('language_id')->params(array('onchange'=>"Lct.redirect('".	url::site('admin/document/category_edit/'.$id) ."?language_id='+this.value);"));	
				$p = $form->field('language_id')->params();
				
			};
		};
		//echo \Debug::vars($bind);
		$this->view->setFile('admin/document/categoryedit');
		$this->edit('models\Documentcategory','documentcategory', $id,null,$events);	
	}
	
	public function action_category_delete($id){ 
		$record = Doctrine::em('s')->getRepository('models\Documentcategory')->findOneById($id);
		if(empty($record)){
			throw new HTTP_Exception_404('Document category was not found');
		}
		$node = new DoctrineExtensions\NestedSet\NodeWrapper($record,Controller_Admin_Document::_getCategoryManager());
		$parentId = $node->getParent()->getId();
		$node->delete();
		Request::current()->redirect(url::site('admin/document/category_edit/'.$parentId)); 				
	}

	
	public function action_create($parent,$title){
		$m = self::_getCategoryManager();
		$new = new models\Documentcategory();
		$new->setIdtext($title);		
		$parent = Doctrine::em('s')->getRepository('models\Documentcategory')->findOneByIdtext($parent);
		echo $parent;
		$parentNode = new DoctrineExtensions\NestedSet\NodeWrapper($parent,$m);
		$parentNode->addChild($new);
		echo "added";		
	}	
	
	public function action_list(){
		self::$_categoryId = Request::current()->query('category_id');
		//echo Controller_Admin_Document::getCategoryId();
		//die;
		Debug::log('controler', self::$_categoryId);
		$this->view->setFile('admin/document/list');
		$this->view->lister = $this->getDocumentListerWidget();
	}
	
	protected function _checkDocumentCategoryPermissions($action){
		$result = Lct::client()->checkRights('Document::'.$action,'category_id='.self::$_categoryId,false);
		if($result === false){
			Helper_JS::alert('_{You are not permitted to '.$action.' a document here}!');
			return false;
		}else
		if($result === null){
			Helper_JS::alert('_{You have no rights associated for '.$action.'-ing a document here}!');
			return true;
		};
		return true;
	}
	
	public function action_edit($documentId,$language = null){
		
		LctLayout::instance()->addJS('tiny_mce/jquery.tinymce');
		
		if($language == null){
			$language = Lct::client()->getLanguage('idText');
		}
		$langCfg = Kohana::config('db_language.'.$language);
		if(empty($langCfg))throw new HTTP_Exception_400('The specified language does not exist');
		
		$this->view->setFile('admin/document/edit');		

		if($documentId == 'new'){
			$this->view->form = Uniform::factory('Document');
			self::$_categoryId = Request::current()->query('category_id');
			if(!self::$_categoryId)self::$_categoryId = Request::current()->post('documentcategory_id');
			if(!self::$_categoryId)throw new HTTP_Exception_400('Category id must be defined');
			if(!$this->_checkDocumentCategoryPermissions('create'))return;
			
			$document = new models\Document();
			if(count(Request::current()->post())){
				if($this->view->form->bind(Request::current()->post())->check()){
					$document->bind($this->view->form->as_array());
					Doctrine::em('m')->persist($document);
					Doctrine::em('m')->flush();
					Request::current()->redirect(url::site('admin/document/edit/'.$document->getId().'/'.$language));
				}else{
					Helper_JS::alert('_{Error in your submitted form data}');
				}				
			}else{
				$this->view->form->bind(array(
					'id'=>'new',
					'documentcategory_id'=>Request::current()->query('category_id'),	
				));
			}		
		}else{
			$languageId = Kohana::config('db_language.'.$language.'.id');
			$this->view->form = Uniform::factory('Documentcontent');
			$document = Doctrine::em('s')->getRepository('models\Document')->findOneById($documentId);						
			if(empty($document)) throw new HTTP_Exception_404('The requested document was not found: '.$documentId);	
			self::$_categoryId = $document->getDocumentcategory()->getId();		
			if(!$this->_checkDocumentCategoryPermissions('update'))return;
			$documentContent = Doctrine::em('s')->find('models\Documentcontent',array(
				'language'=>$languageId,
				'document'=>$document->getId(),
			));
			if(empty($documentContent)){
				$documentContent = new models\Documentcontent();
				$documentContent->setDocument($document);
				$isNew = true;
			}else{
				$isNew = false;
			};
			if(count(Request::current()->post())){
				if($this->view->form->bind(Request::current()->post())->check(true)){
					$documentContent->bind($this->view->form->as_array());
					$documentContent->setDocument($document);
					$documentContent->setLanguage(Doctrine::em('s')->getReference('models\Language',$languageId));
					Doctrine::em('m')->persist($documentContent);
					Doctrine::em('m')->flush();
					Helper_JS::alert('_{Document content has been saved}.');
				}else{
					Helper_JS::alert('_{Error in your submitted form data}.<br>_{Errors}:'.json_encode($this->view->form->getErrors()));
					
				}
			}else{
				if($isNew){
					$documentContent->setIdtext($document->getIdtext());
					$documentContent->setPublic($document->getDocumentcategory()->getPublic());
					$documentContent->setLanguage(Doctrine::em('s')->getReference('models\Language',$languageId));
					Helper_JS::alert('_{Document was created}._{Now you can compose the content of it in any language}.');	
				};			
				$this->view->form->bind($documentContent);
			};
			$this->view->document = $document;	
			$this->view->language = $language;		
		};	
	}
	
	public function action_delete($id){
		$document = Doctrine::em('s')->find('models\Document',$id);
		if(empty($document)){
			throw new HTTP_Exception_404('The requested document was not found');
		}
		$categoryId = $document->getDocumentcategory()->getId();
		$url = url::site('admin/document/list').url::query(array('category_id'=>$categoryId));
		Doctrine::em('m')
			->createQuery('DELETE models\Documentcontent dc WHERE  dc.document = :document')
			->setParameter('document',$document->getId())
			->execute();
		
		Doctrine::em('m')->remove($document);
		Doctrine::em('m')->flush();
		Request::current()->redirect($url);				
	}
	
	public function action_titleslugizer(){
		echo json_encode(LctModel::slugize('models\Documentcontent',Request::current()->query()));	
	}
}