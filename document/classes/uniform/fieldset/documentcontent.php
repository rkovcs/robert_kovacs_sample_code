<?php

class Uniform_Fieldset_Documentcontent extends Uniform_Fieldset {
	
	public function __construct()
	{
	    parent::__construct();
	    $this
	    	->add_field('id')->hname('id')->type('hidden')
	    	->add_field('documentcategory_id')->hname('documentcategory')->type('hidden')
	    	->add_field('document_id')->hname('_{document-id}')->type('hidden')
	    	->add_field('user_id')->hname('_{user}')->type('hidden')
	    	->add_field('language_id')->hname('_{language}')->type('hidden')
	    	->add_field('idtext')->hname('_{text-identifier}')->params(array(
	    		'size'=> 30,  
	    		'tooltip'=>'_{text identifier for URLs},<br/>_{this should be given in your language}',  		
	    	))->type('string')
	    	->add_field('title')->hname('_{title}')->params(array('size'=> 30,))->type('string')
	    	->add_field('createdat')->hname('_{createdat}')->type('datetime')->params(array('size'=> 30,'class'=>'datetimepicker','type'=>'hidden'))
	    	->add_field('modifiedat')->hname('_{modifiedat}')->type('datetime')->params(array('size'=> 30,'class'=>'datetimepicker','type'=>'hidden'))	    	
	    	->add_field('description')->hname('_{description}')->type('text')
	    	->add_field('keywords')->hname('_{keywords}')->type('text')
	    	->add_field('public')->hname('&nbsp;')->type('checkbox')->suffix('_{public}')	    		    	
	    	->add_field('content')->hname('_{content}')->type('text')->params(array(
	    		'class'=>'tinymce',
	    	))
	    	->add_field('plaincontent')->hname('_{plaincontent}')->type('hidden')
	    ;        
	}
}