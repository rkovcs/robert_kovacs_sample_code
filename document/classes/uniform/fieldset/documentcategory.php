<?php

class Uniform_Fieldset_Documentcategory extends Uniform_Fieldset {
	
	public function __construct()
	{
	    parent::__construct();
	    $this
	    	->add_field('id')->hname('id')->params(array('size'=> 30,'type'=>'hidden'))
	    	->add_field('idtext')->hname('_{text-identifier}')->params(array(
	    		'size'=> 30,
	    		'tooltip'=>'_{text identifier for the system}, _{treat it like the filename},<br/>_{this should be given in English}',
	    	))
	    	->add_field('name')->hname('_{name}')->params(array(
	    		'size'=>20,
	    	))
	    	->add_field('language_id')->hname('_{language}')->type('select')->params(array(
	    		'options'=>'SELECT id,name FROM language',
	    		'class'=>'cs_m_gray lctselect',
	    	)) 
	    	->add_field('public')->hname('&nbsp;')->type('checkbox')->suffix('public')   
	        ;	        
	}
}