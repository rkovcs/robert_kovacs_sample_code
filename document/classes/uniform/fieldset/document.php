<?php

class Uniform_Fieldset_Document extends Uniform_Fieldset {
	
	public function __construct()
	{
	    parent::__construct();
	    $this
	    	->add_field('id')->hname('id')->type('hidden')
	    	->add_field('idtext')->hname('_{text-identifier}')->params(array(
	    		'size'=> 30,
	    		'tooltip'=>'_{text identifier for the system}, _{treat it like the filename},<br/>_{this should be given in English}',
	    	))->type('string')
	    	->add_field('documentcategory_id')->hname('_{document-category-id}')->type('hidden');	 
 
	}
}