<?php

class Uniform_Form_Documentcategory extends Uniform_Form {
	public $fieldsets = array(
	    'documentcategory' => array(
	    	'id', 'idtext', 'name','language_id','public',
		),
	);
	
	public function initialize()
	{
		$self = &$this;
		
	}
	
	public function bind($bind = null, $useFilter = FALSE ){
		parent::bind($bind,$useFilter);		
		if($bind instanceof LctModel)$bind = $bind->toArray(false);
		if(!$bind['id']){
			$this->field('name')->params(array('type'=>'hidden'));
			$this->field('language_id')->params(array('type'=>'hidden'));
		}else{
			$this->field('name')->params(array('type'=>'string'));
			$this->field('language_id')->params(array('type'=>'string'));
			$this->field('name')->rule('not_empty');	
		}
		return $this;		
	}
}