<?php

class Uniform_Form_Document extends Uniform_Form {
	public $fieldsets = array(
	    'document' => array(
	    	'id','idtext', 'documentcategory_id'
		),
	);
	
	public function initialize(){
		$self = &$this;
	    $this->field('idtext')->rule('not_empty');
	    $this->field('idtext')->rule(function($value)use($self){	    	  	
	    	$docs = Doctrine::em('s')
	    		->createQuery('SELECT d.idtext FROM models\Document d WHERE d.idtext = :id')
	    		->setParameter('id',$value)
	    		->getResult();
	    	if(count($docs)){
	    		$self->field('idtext')->add_errors('not_unique');
	    		return false;
	    	}else{
	    		return true;
	    	};	    	
	    });	    
	}
}