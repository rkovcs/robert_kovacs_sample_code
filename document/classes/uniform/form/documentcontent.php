<?php

class Uniform_Form_DocumentContent extends Uniform_Form {
	public $fieldsets = array(
	    'documentcontent' => array(
	    	'id','idtext', 'createdat', 'modifiedat', 'document_id', 'language_id','title','content','public','description','keywords','user_id',
	    	'documentcategory_id',
			'plaincontent',
		),
	);
	
	public function initialize(){		
		$self = &$this;   
		$this->field('title')->rule('not_empty');
		$this->field('idtext')->rule(function()use($self){
			if($self->field('idtext')->value()){
				$id = $self->field('id')->value();
				$record = Doctrine::em('s')->find('models\Documentcontent',$id);
				if(!$record)$record = new models\Documentcontent();
				$record->setIdtext($self->field('idtext')->value());
				if(!$record->isFieldUnique('idtext')){
					$self->field('idtext')->addError('not_unique');
					return false;
				}
			}else{
				$result = LctModel::slugize('models\Document',array(
					'slug'=>$self->field('idtext')->value(),
					'targetField'=>'idtext',
					'id'=>$self->field('id')->value(),
				));
				$self->field('idtext')->value($result['slug']);
			}		
			return true;	
		});
		$this->field('plaincontent')->rule(function()use($self){
			$value = $self->field('content')->value();
			$value = html_entity_decode($value, ENT_NOQUOTES, 'UTF-8');			
			$value = strip_tags($value);
			//die($value);
			$self->field('plaincontent')->value($value);
			return true;
		});
		
		/*
		$this->view->form->field('idtext')->prefix(
			'<label></label>'.
			Lct::widget('languageSelector',array(
				'active'=>'hu',
				'url'=>url::site('admin/document/edit/'.$documentId.'/[idText]').url::query(Request::current()->query()),
			)).
			'<br>'
		);
		/**/
	}
}