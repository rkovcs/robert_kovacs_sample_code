<?php

class Widget_Document extends LctWidget{
	protected $_document = null;
	protected $options = null;
	
	public function __construct($condition,$options = array()){
		parent::__construct($condition);
		$this->options = array_merge(array(
		),is_array($options) ? $options : array() );
		
		
		if(is_string($condition))$condition = array('url'=>$condition);
		if(isset($condition['url'])){
			$this->_document = Doctrine::em('s')->createQuery(
				"SELECT dc FROM models\Documentcontent dc JOIN dc.document d WHERE dc.idtext=:url "			
			)->setParameter('url',$condition['url'])
			->getResult();
			//echo Debug::vars($this->_document);
		}else
		if(isset($condition['idtext'])){
			if(isset($condition['language'])){
				$languageId = Kohana::config('db_language.'.$condition['language'].'.id');
			}else{
				$languageId = Lct::client()->getLanguageId();
			};
			$this->_document = $this->document = Doctrine::em('s')->createQuery(
				"SELECT dc FROM models\Documentcontent dc JOIN dc.document d WHERE d.idtext=:idtext AND dc.language = :language "			
			)->setParameter('idtext',$condition['idtext'])
			->setParameter('language',$languageId)
			->getResult();
		}else{
			throw new Exception('(idtext [AND language]) OR url must be passed');
		};
		if(empty($this->_document)){
			throw new Exception('Document does not exist: '.json_encode($condition));
		};
		$this->_document = array_shift($this->_document);
	}
	
	public function getDocument(){
		return $this->_document;
	}
	
	public function getTitle(){
		if(!empty($this->_document)){
			return $this->_document->getTitle();
		}else{
			return null;
		}
	}

	public function getContent(){
		if(!empty($this->_document)){
			$this->_document->storeLocation($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
			return	$this->_document->getContent();
		}else{
			return null;
		}
	}
	
	public function render(){
		$title = $this->getTitle();
		if($title)echo sprintf('<h1 class="document-title">%s</h1><div class="clear"></div>',$title);
		$content = $this->getContent();
		if($content)echo sprintf('<div class="document-content">%s</div>',$content);
	}
	
	public function getAnchors(){
		$anchors = null;
		try{
			$anchors = json_decode($this->_document->getAnchors());
		}catch(Exception $e){;};
		if(!is_array($anchors))return array();
		return $anchors;
	}
	
	public function renderAnchors($params = array()){
		$params = array_merge(array(
			'ul-class'=>'document-anchors',
			'class'=>'document-anchor',
		),$params);		
		$anchors = $this->getAnchors();
		echo '<ul class="'.$params['ul-class'].'">';		
				
		foreach($anchors as $anchor){
			
			echo '<li>'.html::anchor('#'.$anchor->name,$anchor->title,array(
				'class'=>$params['class'],
			)).'</li>';
		}		
		echo '</ul>';
	}
	
	public function getPrintUrl(){
		if(!$this->_document->getPublic()){
			throw new Exception('Cannot provide PRINT-URL for a non-public document');
		}
		return url::site(Kohana::config('document.access-url').$this->_document->getIdtext().'/print');
	}
}