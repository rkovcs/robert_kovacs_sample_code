<?php

class Widget_Document_Lister extends LctWidget{
	protected $_documents = array();
	public function __construct($conditions){
		parent::__construct($conditions);
		$querystr = "
SELECT dc.id,dc.idtext,dc.title,dc.anchors,dc.locations FROM models\Documentcontent dc JOIN dc.document d JOIN d.documentcategory dcat WHERE TRUE = TRUE ";
		$params = array();
		foreach($conditions as $key=>$value){
			switch($key){
				case 'documentcategory':
					$querystr .=' AND dcat.idtext = :documentcategory';
					$params['documentcategory'] = $value;
					break;
				case 'language':
					$querystr .=' AND dc.language = :language';
					$params['language'] = Kohana::config('db_language.'.$value.'.id');
					break;
				default:
					throw new Exception_Internal('Misunderstood condition: '.$key);
			};
		}
		$query = Doctrine::em('s')->createQuery($querystr);		
		$query->setParameters($params);
		$this->_documents = $query->getResult(); 
	}

	public function render($params = array()){
		$params = array_merge(array(
			'ul-class'=>'document-list-widget',
			'class'=>'',
			'url'=>'document/[idtext]',
		),$params);
		echo '<ul class="'.$params['ul-class'].'">';
		if(!empty($this->_documents))foreach($this->_documents as $doc){
			//TODO: url from doc->locations[0] 
			$uri = Lct::substitute($params['url'],$doc);
			echo '<li>'.html::anchor(url::site($uri),$doc['title'],array(
				'class'=>$params['class'],
			)).'</li>';
		}
		echo '</ul>';
	}
}