<?php	

abstract class MailMessage_Interface{

	protected $from;
	protected $replyto;
	protected $to = array();
	protected $addTo = array();
	protected $cc = array();
	protected $bcc = array();
	protected $subject;
	protected $bodies = array('text/plain' => null, 'text/html' => null);
	protected $embeddeds;
	protected $attachments = array();

	/**
	 * @param $address
	 * @param null $name
	 */
	public function from($address, $name = null){
		$this->from = array($address => $name);
	}

	/**
	 * @param $address
	 * @param null $name
	 */
	public function replyto($address, $name = null){
		$this->replyto = array($address => $name);
	}
	/**
	 *
	 * @param array array('address'=>'name',...) | array('address') 
	 */
	public function to(array $address){
		$this->to = $address;
	}

	/**
	 * @param $address
	 * @param $name
	 */
	public function addTo($address, $name){
		$this->addTo[] = array($address=>$name);
	}

	/**
	 * @param $addresses
	 * @param null $name
	 */
	public function cc($addresses, $name = null){
		if(!is_array($addresses)){
			$this->cc[] = array($addresses => $name);
		}else{
			$this->cc = $addresses;
		}
	}

	/**
	 * @param $addresses
	 * @param null $name
	 */
	public function bcc($addresses, $name = null){
		if(!is_array($addresses)){
			$this->bcc[] = array($addresses => $name);
		}else{
			$this->bcc = $addresses;
		}
	}

	/**
	 * @param $subject
	 */
	public function subject($subject){
		$this->subject = $subject;
	}

	/**
	 * @param $content
	 * @param string $type
	 */
	public function addBody($content, $type = "text/plain"){
		$this->bodies[$type] = $content;
	}

	/**
	 * @param array $searchandreplace
	 */
	public function embed(array $searchandreplace){
		$this->embeddeds = $searchandreplace;
	}

	/**
	 * @param $filePath
	 */
	public function attach($filePath){
		$this->attachemnts[] = $filePath;
	}
		
	// Functions to implement in the driver ___________________________________
	abstract public function body($type = "text/plain");
	abstract public function compile();
	abstract public function __toString();

}

abstract class MailerDriver_Interface {

	protected $smtpServers;
	protected $sendmailCommand;
	protected $mailers = array('smtp' => false, 'sendmail' => false, 'native' => false);
	protected $isRotate;
	protected $throttling = array('mails' => null, 'bytes' => null);
	protected $defaultFrom;
	protected $replacements = array();

	/**
	 * @param array $replacement
	 */
	public function setReplacement(array $replacement){
		$this->replacements = $replacement;
	}

	/**
	 * @param $host
	 * @param $port
	 * @param $username
	 * @param $password
	 * @param $encryption
	 */
	public function addSmtp($host, $port, $username, $password, $encryption){
		$this->smtpServers[] = array(
			'hostname' => ($host) ? $host : 'localhost',
			'port' => ($port) ? $port : '25',
			'username' => $username,
			'password' => $password,
			'encryption' => $encryption
		);
	}

	/**
	 * @param $command
	 */
	public function setSendmail($command){
		$this->sendmailCommand = $command;
	}

	/**
	 * @param bool $smtp
	 * @param bool $sendmail
	 * @param bool $native
	 */
	public function mailers($smtp = false, $sendmail = false, $native = false){
		if($smtp) $this->mailers['smtp'] = true;
		if($sendmail) $this->mailers['sendmail'] = true;
		if($native) $this->mailers['native'] = true;
	}

	/**
	 * @param bool $isRotate
	 */
	public function isRotate($isRotate = false){
		$this->isRotate = $isRotate;
	}

	/**
	 * @param $mailsPerMinute
	 * @param $bytesPerMinute
	 */
	public function throttling($mailsPerMinute, $bytesPerMinute){
		$this->throttling['mails'] = $mailsPerMinute;
		$this->throttling['bytes'] = $bytesPerMinute;
	}

	/**
	 * @param $address
	 * @param $name
	 */
	public function defaultFrom($address, $name){
		$this->defaultFrom[$address] = $name;
	}

	/**
	 * Functions to implement in the driver
	 * @param $mail
	 * @return mixed
	 */
	abstract public function send($mail);

	/**
	 * Returns an empty mail object
	 * @return mixed
	 */
	abstract public function newmail();

}