<?php	

abstract class MailMessage_Interface{

	protected $from;
	protected $replyto;
	protected $to = array();
	protected $addTo = array();
	protected $cc = array();
	protected $bcc = array();
	protected $subject;
	protected $bodies = array('text/plain' => null, 'text/html' => null);
	protected $embeddeds;
	protected $attachments = array();

	public function from($address, $name = null){
		$this->from = array($address => $name);
	}

	public function replyto($address, $name = null){
		$this->replyto = array($address => $name);
	}
	/**
	 *
	 * @param array array('address'=>'name',...) | array('address') 
	 */
	public function to(array $address){
		$this->to = $address;
	}
	
	public function addTo($address, $name){
		$this->addTo[] = array($address=>$name);
	}

	public function cc($addresses, $name = null){
		if(!is_array($addresses)){
			$this->cc[] = array($addresses => $name);
		}else{
			$this->cc = $addresses;
		}
	}

	public function bcc($addresses, $name = null){
		if(!is_array($addresses)){
			$this->bcc[] = array($addresses => $name);
		}else{
			$this->bcc = $addresses;
		}
	}

	public function subject($subject){
		$this->subject = $subject;
	}

	public function addBody($content, $type = "text/plain"){
		$this->bodies[$type] = $content;
	}

	public function embed(array $searchandreplace){
		$this->embeddeds = $searchandreplace;
	}
	
	public function attach($filePath){
		$this->attachemnts[] = $filePath;
	}
		
	// Functions to implement in the driver ___________________________________
	abstract public function body($type = "text/plain");
	abstract public function compile();
	abstract public function __toString();

}

abstract class MailerDriver_Interface {
	
	protected $smtpServers;
	protected $sendmailCommand;
	protected $mailers = array('smtp' => false, 'sendmail' => false, 'native' => false);
	protected $isRotate;
	protected $throttling = array('mails' => null, 'bytes' => null);
	protected $defaultFrom;
	protected $replacements = array();

	public function setReplacement(array $replacement){
		$this->replacements = $replacement;
	}	
	
	public function addSmtp($host, $port, $username, $password, $encryption){
		$this->smtpServers[] = array(
			'hostname' => ($host) ? $host : 'localhost',
			'port' => ($port) ? $port : '25',
			'username' => $username,
			'password' => $password,
			'encryption' => $encryption
		);
	}

	public function setSendmail($command){
		$this->sendmailCommand = $command;
	}

	public function mailers($smtp = false, $sendmail = false, $native = false){
		if($smtp) $this->mailers['smtp'] = true;
		if($sendmail) $this->mailers['sendmail'] = true;
		if($native) $this->mailers['native'] = true;
	}

	public function isRotate($isRotate = false){
		$this->isRotate = $isRotate;
	}

	public function throttling($mailsPerMinute, $bytesPerMinute){
		$this->throttling['mails'] = $mailsPerMinute;
		$this->throttling['bytes'] = $bytesPerMinute;
	}

	public function defaultFrom($address, $name){
		$this->defaultFrom[$address] = $name;
	}
	
	// Functions to implement in the driver
	abstract public function send($mail);

	// Returns an empty mail object
	abstract public function newmail();

}