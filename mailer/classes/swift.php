<?php	

require Kohana::find_file('classes', 'mailerdriveri');

class MailMessage_Swift extends MailMessage_Interface{	

	
	public function compile(){
		$mail = Swift_Message::newInstance();
		
		// sender
		$mail->setFrom(array(key($this->from)=>current($this->from)));	
		// set reaply to
		if(isset($this->replyto))
			$mail->setReplyTo(array(key($this->from)=>current($this->from)));
		
		// set subject
		$mail->setSubject($this->subject);

		// embed objects into the bodies

		// set body (with embeddeds if needed)
		if($this->bodies['text/plain'] && !$this->bodies['text/html']) 
			$mail->setBody($this->bodies['text/plain']);

		if(!$this->bodies['text/plain'] && $this->bodies['text/html']){
			$mail->setBody($this->bodies['text/html']);
			$mail->setContentType("text/html");
		}

		// attach files
		foreach($this->attachments AS $attachPath){
			$mail->attach(Swift_Attachment::fromPath($attachPath));
		}
		
		// recepients
		
		if(count($this->to)>0){
			$mail->setTo($this->to);
		}
		
		
		
		
		
		if(is_array($this->addTo)){
			foreach($this->addTo AS $item){
				$mail->addTo(key($item),current($item));
			}
		}
		
		foreach($this->cc as $item){
			$mail->addCc(key($item), current($item));
		}

		// add bcc
		foreach($this->bcc as $item){
			$mail->addBcc(key($item), current($item));
		}		

	
		return $mail;

	}
	

	public function body($type = "text/plain"){}

	public function __toString(){
		if($this->bodies['text/plain'])
			return $this->bodies['text/plain'];
		if($this->bodies['text/html'])
			return $this->bodies['text/html'];
		return '';
	}

}

class MailerDriver_Swift extends MailerDriver_Interface {
	
	protected $swift;
	
	public function connect(){
		
		require Kohana::find_file('classes', 'swift/lib/swift_required');
		
		$transports = array();		
		// create smtp connections if needed
		if($this->mailers['smtp']){
			foreach($this->smtpServers as $server){
				$transport = Swift_SmtpTransport::newInstance($server['hostname'], $server['port'],$server['encryption']);

				if($server['username'] && $server['password']){
					$transport->setUsername($server['username']);
					$transport->setPassword($server['password']);
				}

				$transports[] = $transport;
			}
		}

		// create sendmail connection if needed
		if($this->mailers['sendmail']){
			$transports[] = Swift_SendmailTransport::newInstance($this->sendmailCommand);
		}

		// create native connection if needed
		if($this->mailers['native']){
			$transports[] = Swift_MailTransport::newInstance();
		}
		//print_r($transports); die;
		// create swift mailer
		if($this->isRotate){
			$this->swift = Swift_LoadBalancedTransport::newInstance($transports);	
		}else{
			$this->swift = Swift_Mailer::newInstance($transports[0]);
		}

		// set throttling
		if($this->throttling['mails']){
			$this->swift->registerPlugin(new Swift_Plugins_ThrottlerPlugin($this->throttling['mails'], Swift_Plugins_ThrottlerPlugin::MESSAGES_PER_MINUTE));
		}

		if($this->throttling['bytes']){
			$this->swift->registerPlugin(new Swift_Plugins_ThrottlerPlugin($this->throttling['bytes'], Swift_Plugins_ThrottlerPlugin::BYTES_PER_MINUTE));
		}
		
		
		/* set DecoratorPugin
		 *  
		 * Often there’s a need to send the same message to multiple recipients, but with tiny variations such as the recipient’s
		 * name being used inside the message body. The Decorator plugin aims to provide a
		 * solution for allowing these small differences.
		 */
		
		if(count($this->replacements)>0){
			$decorator = new Swift_Plugins_DecoratorPlugin($this->replacements);
			$this->swift->registerPlugin($decorator);
		}
		
	}
	
	public function send($mail){
		if(!isset($this->swift)){
			$this->connect();
		}
		return $this->swift->send($mail->compile());
	}

	public function last_error(){
		$logger = new Swift_Plugins_Loggers_EchoLogger();
		$this->swift->registerPlugin( new Swift_Plugins_LoggerPlugin($logger));		
		return $logger->dump();
	}

	public function newmail(){
	
		$mail = new MailMessage_Swift();
		reset($this->defaultFrom);
		$mail->from(key($this->defaultFrom), current($this->defaultFrom));
		return $mail;
		
	}
}