<?php	

class Mailer{
	
	protected $driver;

	/**
	 *
	 */
	public function __construct(){
		$config = Kohana::config('mailer');
		
		switch($config['driver']){
			default:
			case 'swift':
				require Kohana::find_file('classes', 'swift');
				$this->driver = new MailerDriver_Swift();
				break;
		}
		$defaultFrom = explode('|', $config['defaultFrom']);
		$this->driver->defaultFrom(array_shift($defaultFrom), implode('|', $defaultFrom));

		$this->driver->throttling($config['mailsPerMinute'], $config['bytesPerMinute']);

		$this->driver->isRotate($config['isRotate']);

		$mailers = explode('|', $config['mailers']);
		$smtp = (in_array('smtp', $mailers)) ? true : false;
		$sendmail = (in_array('sendmail', $mailers)) ? true : false;
		$native = (in_array('native', $mailers)) ? true : false;

		$this->driver->mailers($smtp, $sendmail, $native);

		if($smtp){
			foreach(explode('|', $config['smtpServers']) as $server){
				$params = array(null, null, null, null);
				$params = array_merge(explode(':', $server), $params);
				$this->driver->addSmtp($params[0], $params[1], $params[2], $params[3], $params[4]);
			}
		}

		if($sendmail && $config['sendmailCommand']){
			$this->driver->set_sendmail($config['sendmailCommand']);
		}
	}

	/**
	 * @return mixed
	 */
	public function newmail(){
		return $this->driver->newmail();
	}

	/**
	 * @param $mail
	 * @return mixed
	 */
	public function send($mail){
		return $this->driver->send($mail);
	}

	/**
	 * @param $replacement
	 * @return mixed
	 */
	public function setReplacement($replacement){
		return $this->driver->setReplacement($replacement);
	}

	/**
	 *
	 */
	public static function check_requirements(){}
}