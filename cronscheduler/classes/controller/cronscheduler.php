<?php
/**
 * 
 */
class Controller_CronScheduler extends LctController{
	
	private $tasks;

	/**
	 *
	 */
	public function before(){
		$this->tasks = array();
		ini_set('max_execution_time', 2*(60*60));	// 2 hour running
		flush(); 									// very important for stream_select !
	}

	/**
	 *
	 */
	public function action_index(){
				
		//$activeTasks = models\Cronscheduler::getActiveTasks();
		// Check if there is a task to run
		$tasks2run = array();
		models\Cronthreads::cleanRunnedTasks();
		$concurrency = array();
		
		foreach(models\Cronthreads::getConcurrency() as $row){
		
			$concurrency[$row['task']] = $row['concurrency'];
		}
       
		$regex = date('/^(i|\\\*) (H|\\\*) (d|\\\*) (N|\\\*) (m|\\\*)$/');

		foreach(models\Cronscheduler::getActive() as $task){
			// Filter out the tasks (timing & concurrency)
			if(preg_match($regex, $task['timing'])){ // timing                            
				if(isset($concurrency[$task['task']])){ // concurrency
					if ($concurrency[$task['task']] < $task['concurrency']){
						$tasks2run[] = $task;
					}
				}else{                                       
					$tasks2run[] = $task;
				}
			}
		}
		// Return if no task to run
		if(empty($tasks2run)) return;

		//ob_end_clean();
		//ob_implicit_flush();

		// start tasks


		foreach($tasks2run as $task){
			
			$taskItems = new models\Cronthreads;
			$date = new \DateTime('now');
			$task['startedAt'] = $date->format("Y-m-d H:i:s");
			$taskItems->setStartedat($task['startedAt']);
			$task['finishedAt'] = NULL;
			$taskItems->setFinishedat($task['finishedAt']);
		
			$this->tasks[$task['task']] = $task;
			
			$stream = stream_socket_client($_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'], $errno, $errstr, ini_get("default_socket_timeout"), STREAM_CLIENT_ASYNC_CONNECT|STREAM_CLIENT_CONNECT);	
			fwrite($stream, 'GET '.Url::site().$task['task']." HTTP/1.0\r\nHost: ".$_SERVER['SERVER_NAME']."\r\n\r\n");
			
			/*
				$stream = fopen(url::site($task['task'], NULL, 'http'), 'r');
				stream_set_blocking($stream, 0);
			*/
	
			$taskItems->setTask($task['task']);
			
			Doctrine::em('m')->persist($taskItems);
			
			Doctrine::em('m')->flush();			
			
			$this->tasks[$task['task']]['stream'] = $stream;
		}

		$null = NULL;
		
		// End tasks
		while(count($this->tasks)){
			$waiting_time = floor((1 / count($this->tasks))*1000000);
			foreach($this->tasks as $task){
				$read = array($task['stream']);
				stream_select($read, $null, $null, 0, $waiting_time);
				if(count($read)){
					//print_r($read);
					fread($task['stream'], 8192);
					if(feof($task['stream'])){
						unset($this->tasks[$task['task']]);
						fclose($task['stream']);
						// update thread
						$date = new \DateTime('now');
						$task['finishedAt'] = date('Y-m-d H:i:s',max($date->getTimestamp(), (strtotime($task['startedAt'])+25))); // prevent to start same task within a minute
						models\Cronthreads::updateThread($task);
					}
				}
			}			
		}
	}	
}
